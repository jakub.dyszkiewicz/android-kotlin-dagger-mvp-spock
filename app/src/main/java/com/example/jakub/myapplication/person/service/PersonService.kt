package com.example.jakub.myapplication.person.service

import com.example.jakub.myapplication.person.model.Person
import com.example.jakub.myapplication.person.model.PersonResponse
import retrofit2.Call
import retrofit2.http.GET
import rx.Observable

interface PersonService {

    @GET("?page=3&results=10&seed=abc")
    fun getUsers() : Observable<PersonResponse>

    companion object {
        val URL = "https://randomuser.me/api/"
    }
}