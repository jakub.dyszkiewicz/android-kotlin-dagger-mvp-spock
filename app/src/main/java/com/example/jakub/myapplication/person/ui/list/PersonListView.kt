package com.example.jakub.myapplication.person.ui.list

import com.example.jakub.myapplication.person.model.Person

interface PersonListView {
    fun displayPeople(people: List<Person>)
}