package com.example.jakub.myapplication.person.config

import com.example.jakub.myapplication.person.service.ImageDownloader
import com.example.jakub.myapplication.person.service.PersonService
import com.example.jakub.myapplication.person.ui.list.PersonListPresenter
import com.example.jakub.myapplication.person.ui.list.PersonListView
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PersonListPresenterModule(val view: PersonListView) {

    @Provides @Singleton
    fun providePresenter(service: PersonService): PersonListPresenter =
            PersonListPresenter(view, service, ImageDownloader())
}