package com.example.jakub.myapplication.person.ui.list

import android.graphics.drawable.Drawable
import com.example.jakub.myapplication.person.service.PersonService
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.util.Log
import com.example.jakub.myapplication.person.service.ImageDownloader
import java.net.URL


class PersonListPresenter(val view: PersonListView, val service: PersonService, val imageDownloader: ImageDownloader) {

    fun start() = loadPeople()

    fun loadPeople() {
        service.getUsers()
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(Schedulers.immediate())
//                .subscribeOn(Schedulers.immediate())
                .flatMap { Observable.from(it.results) }
                .doOnNext { it.picture.bitmap = imageDownloader.download(it.picture.medium) }
                .toList()
//                .toBlocking()
//                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it.size
//                    view.displayPeople(it)
                }, { error -> Log.e("asd", "Error", error) })
    }
}