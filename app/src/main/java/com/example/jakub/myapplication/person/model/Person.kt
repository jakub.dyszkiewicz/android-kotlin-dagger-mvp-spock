package com.example.jakub.myapplication.person.model

import android.graphics.Bitmap

data class PersonResponse(val results: List<Person>)

data class Person(val name: Name, val gender: String, val picture: Picture)

data class Picture(val medium: String, var bitmap: Bitmap?)

data class Name(val title: String, val first: String, val last: String) {
    val fullName = "$title $first $last"
}