package com.example.jakub.myapplication.person.service

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.net.URL

class ImageDownloader {
    fun download(url: String): Bitmap = BitmapFactory.decodeStream(URL(url).openConnection().inputStream)
}