package com.example.jakub.myapplication

import android.app.Application
import com.example.jakub.myapplication.config.DaggerApplicationComponent

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        val graph = DaggerApplicationComponent.create()
        graph.inject(this)
    }
}