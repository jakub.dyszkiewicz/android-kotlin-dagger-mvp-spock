package com.example.jakub.myapplication.person.ui.list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.jakub.myapplication.R
import com.example.jakub.myapplication.person.model.Person
import org.jetbrains.anko.find
import org.jetbrains.anko.imageBitmap

class PersonAdapter : RecyclerView.Adapter<PersonAdapter.PersonViewHolder>() {
    private var people: List<Person> = listOf()

    fun swapPeople(people: List<Person>) {
        this.people = people
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
        val view = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.person_item, parent, false)
        return PersonViewHolder(view)
    }

    override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
        val person = people[position]
        holder.bind(person)
    }

    override fun getItemCount(): Int = people.size

    class PersonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name : TextView = itemView.find(R.id.text_person_item_name)
        val image: ImageView = itemView.find(R.id.image_person_item)

        fun bind(person: Person) {
            name.text = person.name.fullName
            image.imageBitmap = person.picture.bitmap
        }
    }
}