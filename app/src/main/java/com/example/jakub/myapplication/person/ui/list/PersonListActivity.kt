package com.example.jakub.myapplication.person.ui.list

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.jakub.myapplication.R
import com.example.jakub.myapplication.extensions.bindView
import com.example.jakub.myapplication.person.config.DaggerPersonComponent
import com.example.jakub.myapplication.person.config.PersonListPresenterModule
import com.example.jakub.myapplication.person.model.Person
import javax.inject.Inject

class PersonListActivity : AppCompatActivity(), PersonListView {

    val recyclerView: RecyclerView by bindView(R.id.main_recycler_view)
    val adapter = PersonAdapter()

    @Inject lateinit var presenter: PersonListPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        DaggerPersonComponent.builder()
                .personListPresenterModule(PersonListPresenterModule(this))
                .build()
                .inject(this)
        presenter.start()
    }

    override fun displayPeople(people: List<Person>) {
        adapter.swapPeople(people)
    }

}

