package com.example.jakub.myapplication.person.config

import com.example.jakub.myapplication.config.RetrofitModule
import com.example.jakub.myapplication.person.ui.list.PersonListActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
        PersonModule::class,
        PersonListPresenterModule::class
))
interface PersonComponent {
    fun inject(activity: PersonListActivity)
}