package com.example.jakub.myapplication.person.config

import com.example.jakub.myapplication.config.RetrofitModule
import com.example.jakub.myapplication.person.service.PersonService
import com.example.jakub.myapplication.person.ui.list.PersonListPresenter
import com.example.jakub.myapplication.person.ui.list.PersonListView
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = arrayOf(RetrofitModule::class))
class PersonModule {

    @Provides @Singleton
    fun providePersonService(retrofit: Retrofit): PersonService = retrofit.create(PersonService::class.java)
}