package com.example.jakub.myapplication

import org.junit.Test
import rx.Observable

class SampleTest {

    @Test
    fun sampleTest() {
        Observable.just(1)
                .map { it * 2 }
                .doOnNext { print("$it") }
                .toList()
                .subscribe({
                    print("asd")
                }, { error -> })
    }
}