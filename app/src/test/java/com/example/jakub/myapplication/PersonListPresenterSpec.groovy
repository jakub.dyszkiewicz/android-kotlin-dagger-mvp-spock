package com.example.jakub.myapplication

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.example.jakub.myapplication.person.model.Name
import com.example.jakub.myapplication.person.model.Person
import com.example.jakub.myapplication.person.model.PersonResponse
import com.example.jakub.myapplication.person.model.Picture
import com.example.jakub.myapplication.person.service.ImageDownloader
import com.example.jakub.myapplication.person.service.PersonService
import com.example.jakub.myapplication.person.ui.list.PersonListPresenter
import com.example.jakub.myapplication.person.ui.list.PersonListView
import org.junit.Test
import rx.Observable
import spock.lang.Specification
import spock.lang.Subject


class PersonListPresenterSpec extends Specification {

    def view = GroovyMock(PersonListView)
    def personService = GroovyMock(PersonService)
    def imageDownloader = GroovyMock(ImageDownloader)

    @Subject
    def presenter = new PersonListPresenter(view, personService, imageDownloader)

    def 'should load people on start'() {
        List<Person> caught
        given:
            def people = [new Person(new Name("mr", "John", "Doe"), "Man", new Picture("http://picture1.jpg", null)),
                          new Person(new Name("ms", "Anna", "Doe"), "Man", new Picture("http://picture2.jpg", null))]
            personService.getUsers() >> Observable.just(new PersonResponse(people))
            imageDownloader.download("http://picture1.jpg") >> BitmapFactory.decodeFile("res/minimap-xxhdpi/ic_launcher.png")
            imageDownloader.download("http://picture2.jpg") >> BitmapFactory.decodeFile("res/minimap-xxhdpi/ic_launcher.png")
            view.displayPeople(_) >> { caught = it[0] }
        when:
            presenter.start()
        then:
            caught.size() == 2
    }
}